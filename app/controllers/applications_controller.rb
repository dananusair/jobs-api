class ApplicationsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_application, only: [:show, :destroy]
  before_action :set_application_as_seen, only: [:show]
  
  load_and_authorize_resource

  # GET /applications or /applications.json
  def index
    @applications = Application.accessible_by(current_ability)
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render xml: @applications}
      format.json { render json: {applications: @applications}}
    end
  end

  # GET /applications/1 or /applications/1.json
  def show
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render xml: @application}
      format.json { render json: {application: @application}}
    end
  end

  # POST /applications or /applications.json
  def create
    @application = Application.new(application_params)

    respond_to do |format|
      if @application.save
        format.html { redirect_to @application, notice: "Application was successfully created." }
        format.json { render :show, status: :created, location: @application }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1 or /applications/1.json
  def destroy
    @application.destroy
    respond_to do |format|
      format.html { redirect_to applications_url, notice: "Application was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:id])
    end

    def set_application_as_seen
      if current_user.admin? && @application.status == 'Not Seen'
        @application.assign_attributes(status: 'Seen')
        render json: @application.errors, status: :unprocessable_entity if !@application.save
      end
    end

    # Only allow a list of trusted parameters through.
    def application_params
      params.require(:application).permit(:user_id, :job_id)
    end
end
