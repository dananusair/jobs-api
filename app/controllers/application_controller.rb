class ApplicationController < ActionController::Base
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { render json: {error: "You do not have access to this action"}, status: :forbidden }
      format.html { redirect_to root_url, notice: exception.message, status: :forbidden }
      format.js   { render nothing: true, status: :forbidden }
    end
  end

  rescue_from(
    ActionController::RoutingError,
    ActionController::ParameterMissing,
    ActiveRecord::RecordNotFound) do |exception|
    respond_to do |format|
      format.json { render json: { error: exception.message }, status: :bad_request }
      format.html { redirect_to root_url, notice: exception.message, status: :bad_request }
      format.js   { render nothing: true, status: :bad_request }
    end
  end

  protected
    def json_request?
      request.format.json?
    end
end
