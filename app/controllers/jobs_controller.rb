class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  load_and_authorize_resource

  # GET /jobs or /jobs.json
  def index
    search = params[:search]
    if !search.blank?
      @jobs = Job.search(search)
      # Filtering on search kept not returning anything, so I commented it
      # @jobs = Job.search(search, where: {expiry_date: {gt: DateTime.now}})
    else
      @jobs = Job.where('expiry_date > ?', DateTime.now)
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render xml: @jobs}
      format.json { render json: {jobs: @jobs}}
    end
  end

  # GET /jobs/1 or /jobs/1.json
  def show
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render xml: @job}
      format.json { render json: @job}
    end
  end

  # GET /jobs/new
  def new
    @job = Job.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs or /jobs.json
  def create
    @job = Job.new(job_params.merge({user_id: current_user.id}))

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: "Job was successfully created." }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1 or /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: "Job was successfully updated." }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1 or /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: "Job was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def job_params
      params.require(:job).permit(:title, :description, :expiry_date)
    end
end
