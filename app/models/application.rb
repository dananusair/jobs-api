class Application < ApplicationRecord
  STATUS_OPTIONS = ['Seen', 'Not Seen']
  
  belongs_to :user
  belongs_to :job

  validates_uniqueness_of :job_id, scope: :user_id
  validates_inclusion_of :status, in: STATUS_OPTIONS

  validate do
    if user.admin?
      errors.add(:application, "Admin can not create applications")
    end
  end
end
