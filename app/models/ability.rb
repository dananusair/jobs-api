# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
      cannot :create, Application
      cannot :destroy, Application
      cannot :destroy, User
    else
      can :read, Job
      can :read, Application, user: user
      can :update, Application, user: user
      can :destroy, Application, user: user
      can :create, Application
      cannot :destroy, Job
      cannot :create, Job
      cannot :update, Job
    end
  end
end
