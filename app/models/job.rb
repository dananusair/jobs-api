class Job < ApplicationRecord
  searchkick word_middle: [:title, :description]

  has_many :applications, dependent: :destroy
  belongs_to :user
  
  validates :title, :description, presence: true

  after_save do
    Job.reindex
  end

  validate do
    if !user.admin?
      errors.add(:user, "Only admins can create a job")
    end
  end

  def search_data
    {
      title: title,
      description: description
    }
  end
end
