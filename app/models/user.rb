class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :applications, dependent: :destroy

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validate do 
    if self.admin == true && User.exists?(admin: true)
      errors.add(:admin, "there can only be one admin")
    end
  end
end
