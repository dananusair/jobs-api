json.extract! application, :id, :status, :user_id, :job_id, :created_at, :updated_at
json.url application_url(application, format: :json)
