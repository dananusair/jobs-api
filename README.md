## Introduction

Simple app to that allows admins to create job postings and gives the regular users the ability to apply for jobs and track if the admins have seen their applications. Jobs that have exceeded the expiry date shouldn't show up.

## How to use

This project has a simple interface where you have the ability to do most of the work and it also has JSON APIs.

## Heroku

You can access the application through [the heroku app](https://jobs-api-zenhr.herokuapp.com/)

## Devise

Devise is used to handle user registration and basic authentication. Sign up allows for the creation of both job seekers and admin. There can only be one admin.

## Authentication

This project uses basic authentication, when using postman, make sure to use the basic auth header to enter your credentials.

## SearchKick

Searchkick was used in order to search for jobs based on title and description.

## APIs

- ### Jobs
  - Retrieve all (GET) - All users
  ```
    METHOD: GET
    URL: <HOST>/jobs.json
    QUERY_PARAMS: ?search=<STRING>
  ```
  - Retrieve one job (GET) - All users
  ```
    METHOD: GET
    URL: <HOST>/jobs/<JOB_ID>.json
  ```
  - Create job (POST) - Admin
  ```
    METHOD: POST
    URL: <HOST>/jobs.json
    BODY: {
      job: {
        "title": <STR>,
        "description": <TEXT>
      }
    }
  ```
  - Update job (PUT) - Admin
  ```
    METHOD: PUT
    URL: <HOST>/jobs.json
    BODY: {
      job: {
        "title": <STR>,
        "description": <TEXT>
      }
    }
  ```
  - Delete job (DELETE) - Admin
  ```
    METHOD: DELETE
    URL: <HOST>/jobs/<JOB_ID>.json
  ```
- ### Applications
  - Retrieve all (GET) - All users (job seekers can only view their applications)
  ```
    METHOD: GET
    URL: <HOST>/applications.json
  ```
  - Retrieve one application - All users (job seekers can only view their application)
  ```
    METHOD: GET
    URL: <HOST>/applications/<APP_ID>.json
  ```
  - Create application - job seekers
  ```
    METHOD: POST
    URL: <HOST>/applications.json
    BODY: {
      application: {
        "user_id": <INT>,
        "job_id": <INT>
      }
    }
  ```
  - Destroy application - job seekers
  ```
    METHOD: DELETE
    URL: <HOST>/applications/<APP_ID>.json
  ```
