class CreateJobs < ActiveRecord::Migration[6.1]
  def change
    create_table :jobs do |t|
      t.string :title, null: false
      t.text :description, null: false, default: ""
      t.datetime :expiry_date
      t.timestamps
    end
  end
end
