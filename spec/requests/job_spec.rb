require 'rails_helper'
require 'request_helper'
include RequestHelper

RSpec.describe "Jobs", type: :request do
  fixtures :users
  fixtures :jobs
  
  subject {
    Job.new(
      id: 1,
      title: 'title',
      description: 'description',
      user: users(:user1)
    )
  }
  
  describe 'GET index' do
    it 'reaches index path and responds with status :ok' do
      get jobs_path
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET show' do
    it 'reaches the show path and responds with status :ok' do
      subject.save
      get jobs_path, params: { id: subject.id }
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST#create' do
    before(:each) do
      sign_in users(:user1)
    end

    context 'with valid parameters' do
      let(:valid_params) do
        {
          posting: {
            title: subject.title,
            description: subject.title
          }
        }
      end

      it 'creates a new job' do
        post '/jobs.json', params: valid_params
        expect(response).to change(Job, :count).by(1)
      end

      it 'responds to status :created' do
        post jobs_path, params: valid_params
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid parameters' do
      let(:invalid_params) do
        {
          job: {
            title: nil,
            description: nil
          }
        }
      end

      it 'does not create a new posting' do
        expect {
          post jobs_path, params: invalid_params
        }.to_not change(Job, :count)
      end

      it 'expects creation to be unsuccessful' do
        post jobs_path, params: invalid_params
        expect(response).to_not be_successful
      end
    end
  end
  describe 'PUT #update' do
    before(:each) do
      sign_in(users(:user1))
      subject.save
    end

    context 'with valid parameters' do
      it 'successfully updates a job' do
        put jobs_path + '/' + String(subject.id), params: { job: { title: 'new title' } }
        expect(response).to be_successful
      end
    end

    context 'with invalid parameters' do
      it 'updates a job' do
        put jobs_path + '/' + String(subject.id), params: { job: { title: nil } }
        expect(response).to_not be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      sign_in(users(:user1))
      subject.save
    end

    it 'destroys a job' do
      expect {
        delete jobs_path + '/' + String(subject.id)
      }.to change(Job, :count).by(-1)
    end

    it 'responds with status :no_content' do
      delete jobs_path + '/' + String(subject.id)
      expect(response).to have_http_status(:no_content)
    end
  end
end
