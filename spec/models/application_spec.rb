require 'rails_helper'

RSpec.describe Application, type: :model do
  fixtures :users
  fixtures :jobs

  describe 'validations' do
    it 'should be valid with valid attributes' do
      user = users(:user2)
      job = jobs(:job1)
      job = Application.new(user: user, job: job)
      expect(job).to be_valid
    end

    it 'should be create application if user is admin' do
      user = users(:user1)
      job = jobs(:job1)
      application = Application.new(user: user, job: job)
      expect(application).to_not be_valid
    end

    it 'should not create application if user has already applied for job' do
      user = users(:user1)
      job = jobs(:job1)
      application1 = Application.create(user: user, job: job)
      application2 = Application.new(user: user, job: job)
      expect(application2).to_not be_valid
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      assc = described_class.reflect_on_association :user
      expect(assc.macro).to eq :belongs_to
    end

    it 'belongs to job' do
      assc = described_class.reflect_on_association :job
      expect(assc.macro).to eq :belongs_to
    end
  end

  describe '#before_create' do
    it 'should have \'Not Seen\' as the default status' do
      user = users(:user2)
      job = jobs(:job1)
      application = Application.create(user: user, job: job)
      expect(application.status).to eq('Not Seen')
    end
  end
end
