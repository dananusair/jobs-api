require 'rails_helper'

RSpec.describe Job, type: :model do
  fixtures :users
  
  describe 'validations' do
    it 'should be valid with valid attributes' do
      user = users(:user1)
      job = Job.new(user: user, title: 'some title', description: 'some description')
      expect(job).to be_valid
    end

    it 'should be invalid with title equals nil' do
      user = users(:user1)
      job = Job.new(user: user, title: nil, description: 'description')
      expect(job).to_not be_valid
    end

    it 'should be invalid with description equals nil' do
      user = users(:user1)
      job = Job.new(user: user, title: 'title', description: nil)
      expect(job).to_not be_valid
    end

    it 'should be invalid if title was empty string' do
      user = users(:user1)
      job = Job.new(user: user, title: '', description: 'description')
      expect(job).to_not be_valid
    end

    it 'should be invalid if description was empty string' do
      user = users(:user1)
      job = Job.new(user: user, title: 'title', description: '')
      expect(job).to_not be_valid
    end

    it 'should not create a job if user is not admin' do
      user = users(:user2)
      job = Job.new(user: user, title: 'title', description: 'description')
      expect(job).to_not be_valid
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      assc = described_class.reflect_on_association :user
      expect(assc.macro).to eq :belongs_to
    end

    it 'has many applications' do
      assc = described_class.reflect_on_association :applications
      expect(assc.macro).to eq :has_many
    end
  end
end
